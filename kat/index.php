<html>
 <head>
	<meta charset="utf-8" />
	<link href="../common/index.css" type="text/css" rel="stylesheet" />
 </head>
 <body>
	<header>
		<span id="other">
			<a href="../ara">ARA</a>
			<a href="../bel">BEL</a>
			<a href="../rus">RUS</a>
			<a href="../ukr">UKR</a>
		</span>
		<form method="POST" id="ipt">
			<input type="text" name="kat" />
			<span class="btt" onclick="trb1(this.previousElementSibling.value)" />Transkrybuj z języka gruzińskiego</span>
		</form>
	</header>
	<div id="content">
		<div id="settings">
			<form>
				<input type="checkbox" name="setForeign" id="setForeign" />
				<label for="setForeign">nazwisko z języka obcego</label>
			</form>
		</div>
		<div id="container">
			<div id="out"></div>
		</div>
	</div>
	<script>
		var spec = 0;
/*
spec
0 - 
1 - 
*/
		function sep(obj) {
			let xx = obj.value;
			let a = "";
			for(let i = 0; i < xx.length; i++) {
				a = a + xx[i] + " ";
			}
			obj.value = a;
		}
		function trb1(val) {
			let out = "";
			val = "[" + val + "]";
			for(let i = 1; i < val.length - 1; i++)
				out = out + trb(val[i - 1], val[i], val[i + 1] );
			//document.getElementById("out").innerHTML = out;
			let parts = out.split(" ");
			let out2 = "";
			/*for(let i = 0; i < parts.length; i++) {
				out2 += dict(parts[i] ) + " ";
			}*/
			document.getElementById("out").innerHTML = out + "<br/>" + out2;
		}
		function trb(prev, curr, next) {
			let lac = "";
			let currCase = curr;
			curr = curr.toLowerCase();
			prev = prev.toLowerCase();
			next = next.toLowerCase();
			switch(curr ) {
				case "ა":	lac = "a";
							break;
				case "ბ":	lac = "b";
							break;
				case "გ":	lac = "g";
							break;
				case "დ":	lac = "d";
							break;
				case "ე":	lac = "e";
							break;
				case "ვ":	lac = "w";
							break;
				case "ზ":	lac = "z";
							break;
				case "თ":	lac = "t";
							break;
				case "ი":	lac = "i";
							break;
				case "კ":	lac = "k";
							break;
				case "ლ":	lac = "l";
							break;
				case "მ":	lac = "m";
							break;
				case "ნ":	lac = "n";
							break;
				case "ო":	lac = "o";
							break;
				case "პ":	lac = "p";
							break;
				case "ჟ":	lac = "ż";
							break;
				case "რ":	lac = "r";
							break;
				case "ს":	lac = "s";
							break;
				case "ტ":	lac = "t";
							break;
				case "უ":	lac = "u";
							break;
				case "ფ":	lac = "p";
							break;
				case "ქ":	lac = "k";
							break;
				case "ღ":	lac = "gh";
							break;
				case "ყ":	lac = "k";
							break;
				case "შ":	lac = "sz";
							break;
				case "ჩ":	lac = "cz";
							break;
				case "ც":	lac = "c";
							break;
				case "ძ":	lac = "dz";
							break;
				case "წ":	lac = "c";
							break;
				case "ჭ":	lac = "cz";
							break;
				case "ხ":	lac = "ch";
							break;
				case "ჯ":	lac = "dż";
							break;
				case "ჰ":	lac = "h";
							break;
				case " ":	lac = " ";
							break;
				default:	lac = "<span class='not_match'>" + curr + "</span>"
			}
			
			// wielkie litery
			
			return lac;
		}
	</script>
 </body>
</html>