<html>
 <head>
	<meta charset="utf-8" />
	<link href="../common/index.css" type="text/css" rel="stylesheet" />
 </head>
 <body>
	<header>
		<span id="other">
			<a href="../ara">ARA</a>
			<a href="../bel">BEL</a>
			<a href="../rus">RUS</a>
			<a href="../ukr">UKR</a>
		</span>
		<form method="POST" id="ipt">
			<input type="text" name="ukr" />
			<span class="btt" onclick="trb1(this.previousElementSibling.value)" />Transkrybuj z języka rosyjskiego</span>
		</form>
	</header>
	<div id="content">
		<div id="settings">
			<form>
				<input type="checkbox" name="setForeign" id="setForeign" />
				<label for="setForeign">nazwisko z języka obcego</label>
			</form>
		</div>
		<div id="container">
			<div id="out"></div>
		</div>
	</div>
	<script>
		var spec = 0;
/*
spec
0 - 
1 - aktywuje sprawdzanie końcówki -aja / do zmiany na -oj
*/
		function sep(obj) {
			let xx = obj.value;
			let a = "";
			for(let i = 0; i < xx.length; i++) {
				a = a + xx[i] + " ";
			}
			obj.value = a;
		}
		function trb1(val) {
			let out = "";
			val = "[" + val + "]";
			for(let i = 1; i < val.length - 1; i++)
				out = out + trb(val[i - 1], val[i], val[i + 1] );
			//document.getElementById("out").innerHTML = out;
			let parts = out.split(" ");
			let out2 = "";
			/*for(let i = 0; i < parts.length; i++) {
				out2 += dict(parts[i] ) + " ";
			}*/
			document.getElementById("out").innerHTML = out + "<br/>" + out2;
		}
		function trb(prev, curr, next) {
			let lac = "";
			let currCase = curr;
			curr = curr.toLowerCase();
			prev = prev.toLowerCase();
			next = next.toLowerCase();
			switch(curr ) {
				case "а":	lac = "a";
							break;
				case "б":	lac = "b";
							break;
				case "в":	lac = "w";
							break;
				case "г":	lac = "g";
							break;
				case "д":	lac = "d";
							break;
				case "е":	lac = "e";		// dodatkowe okoliczności -> patrz dalej
							break;
				case "ё":	lac = "o";		// dodatkowe okoliczności -> patrz dalej
							break;
				case "ж":	lac = "ż";
							break;
				case "з":	lac = "z";
							break;
				case "и":	lac = "i";		// dodatkowe okoliczności -> patrz dalej
							break;
				case "й":	lac = "j";
							break;
				case "к":	lac = "k";
							break;
				case "л":	lac = "ł";		// dodatkowe okoliczności -> patrz dalej
							break;
				case "м":	lac = "m";
							break;
				case "н":	lac = "n";
							break;
				case "о":	lac = "o";
							break;
				case "п":	lac = "p";
							break;
				case "р":	lac = "r";
							break;
				case "с":	lac = "s";
							break;
				case "т":	lac = "t";
							break;
				case "у":	lac = "u";
							break;
				case "ф":	lac = "f";
							break;
				case "х":	lac = "ch";
							break;
				case "ц":	lac = "c";
							break;
				case "ч":	lac = "cz";
							break;
				case "ш":	lac = "sz";
							break;
				case "щ":	lac = "szcz";
							break;
				case "ъ":	lac = "";		// pomija się
							break;
				case "ы":	lac = "y";
							break;
				case "ь":	lac = "";		// dodatkowe okoliczności -> patrz dalej
							break;
				case "э":	lac = "e";
							break;
				case "ю":	lac = "u";		// dodatkowe okoliczności -> patrz dalej
							break;
				case "я":	lac = "a";		// dodatkowe okoliczności -> patrz dalej
							break;
				case " ":	lac = " ";
							break;
			}
/* samogłoski
А а
О о
И и
У у
Ы ы
Э э
е, ё, ю, я
*/
			if(curr == "е")
			{
				let out = document.getElementById("setForeign").checked;
				if(prev == "[" || prev == " ")
					lac = "Je";
				else if(prev == "ъ" || prev == "ь" || prev == "а" || prev == "о" || prev == "и" || prev == "у" || prev == "ы" || prev == "э"
						|| prev == "е" || prev == "ё" || prev == "ю" || prev == "я" )
					lac = "je";	
				else if(prev == "ж" || prev == "л" || prev == "ц" || prev == "ч" || prev == "ш" || prev == "щ" || out)
					lac = "e";
				else
					lac = "ie";
			}
			
			if(curr == "ё")
			{
				if(prev == "[" || prev == " ")
					lac = "Jo";
				else if(prev == "ъ" || prev == "ь" || prev == "а" || prev == "о" || prev == "и" || prev == "у" || prev == "ы" || prev == "э"
						|| prev == "е" || prev == "ё" || prev == "ю" || prev == "я" )
					lac = "jo";	
				else if(prev == "ж" || prev == "л" || prev == "ц" || prev == "ч" || prev == "ш" || prev == "щ")
					lac = "o";
				else
					lac = "io";
			}
			
			if(curr == "и")
			{
				if(prev == "ъ")
					lac = "ji";	
				else if(prev == "ж" || prev == "ц" || prev == "ш")
					lac = "y";
			}
			
			if(curr == "л" &&			
				(next == "е" || next == "ё" || next == "и" || next == "ь" || next == "ю" || next == "я") ) {
					lac = "l";	
			}
			if(curr == "ю")
			{
				if(prev == "[" || prev == " ")
					lac = "Ju";
				else if(prev == "ъ" || prev == "ь" || prev == "а" || prev == "о" || prev == "и" || prev == "у" || prev == "ы" || prev == "э"
						|| prev == "е" || prev == "ё" || prev == "ю" || prev == "я" )
					lac = "ju";	
				else if(prev == "л")
					lac = "u";
				else
					lac = "iu";
			}
			
			if(curr == "я")
			{
				if(prev == "[" || prev == " ")
					lac = "Ja";
				else if(prev == "ъ" || prev == "ь" || prev == "а" || prev == "о" || prev == "и" || prev == "у" || prev == "ы" || prev == "э"
						|| prev == "е" || prev == "ё" || prev == "ю" || prev == "я" )
					lac = "ja";	
				else if(prev == "л")
					lac = "a";
				else
					lac = "ia";
			}
			
			// PWN 77.B.1 -	zakończenia rosyjskich nazwisk na -ский, -цкий, -ий, -ый 
			//				zapisujemy w tekstach polskich przez -ski, -cki, -i, -y
			// PWN 77.B.4 - rosyjskie nazwiska zakończone na -инский transkrybujemy przez -inski
			if( (prev == "и" || prev == "ы") && curr == "й" && next == "]")
				lac = "";
			
			// PWN 77.B.2 - nazwiska żeńskie na -ая, będące odpowiednikiem męskich na -ой, nie ulegają spolszczeniu.
			//				Zachowujemy je w formie męskiej
			// PWN 77.B.4 - rosyjskie nazwiska zakończone na -инская transkrybujemy przez -inska
			// PWN 77.B.6 - rosyjskie nazwiska żeńskie kończące się na -ская, -цкая transkrybujemy, używając zakończeń -ska, -cka
			if(spec == 1) {
				spec = 0;
				if(next == "]")
					lac = "oj";
				else
					lac = "aja";
			}
			if(spec == 2) {
				spec = 0;
				if(next == "]")
					lac = "a";
				else
					lac = "aja";
			}
			if(curr == "а" && next == "я") {
				if(prev == "к")
					spec = 2;
				else
					spec = 1;
				lac = "";
			}
			
			if(curr != currCase ) {
				let tmp = "";
				for(let i = 0; i < lac.length; i++)
					if(i == 0)
						tmp += lac.charAt(i).toUpperCase();
					else
						tmp += lac.charAt(i);
				lac = tmp;
			}
			return lac;
		}
	</script>
 </body>
</html>