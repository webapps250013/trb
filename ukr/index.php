<?php
	if(isset($_GET["api"] ) ) {
		
	}
?>
<html>
 <head>
	<meta charset="utf-8" />
	<link href="../common/index.css" type="text/css" rel="stylesheet" />
	<title>UKR</title>
 </head>
 <body>
	<header>
		<span id="other">
			<a href="../ara">ARA</a>
			<a href="../bel">BEL</a>
			<a href="../rus">RUS</a>
			<a href="../ukr">UKR</a>
		</span>
		<form method="POST" id="ipt">
			<input type="text" name="ukr" />
			<span class="btt" onclick="trb1(this.previousElementSibling.value)" />Transkrybuj z języka ukraińskiego</span>
		</form>
	</header>
	<div id="content">
		<div id="settings">
			<form>
				<input type="checkbox" name="setPWN78C1" id="setPWN78C1" />
				<label for="setPWN78C1">polszczenie końcówki nazwiska</label>
			</form>
		</div>
		<div id="container">
			<div id="out"></div>
		</div>
	</div>
	<script>
		var spec = 0;
/*
spec
0 - 
1 - 
*/
		function sep(obj) {
			let xx = obj.value;
			let a = "";
			for(let i = 0; i < xx.length; i++) {
				a = a + xx[i] + " ";
			}
			obj.value = a;
		}
		function trb1(val) {
			let out = "";
			val = "[" + val + "]";
			for(let i = 1; i < val.length - 1; i++)
				out = out + trb(val[i - 1], val[i], val[i + 1] );
			document.getElementById("out").innerHTML = out;
		}
		function trb(prev, curr, next) {
			let lac = "";
			let currCase = curr;
			curr = curr.toLowerCase();
			prev = prev.toLowerCase();
			next = next.toLowerCase();
			switch(curr ) {
				case "а":	lac = "a";
							break;
				case "б":	lac = "b";
							break;
				case "в":	lac = "w";
							break;
				case "г":	lac = "h";
							break;
				case "ґ":	lac = "g";
							break;
				case "д":	lac = "d";
							break;
				case "е":	lac = "e";
							break;
				case "є":	lac = "e";		// dodatkowe okoliczności -> patrz dalej
							break;
				case "ж":	lac = "ż";
							break;
				case "з":	lac = "z";
							break;
				case "и":	lac = "y";		// dodatkowe okoliczności -> patrz dalej
							break;
				case "і":	lac = "i";
							break;
				case "ї":	lac = "ji";
							break;
				case "й":	lac = "j";
							break;
				case "к":	lac = "k";
							break;
				case "л":	lac = "ł";		// dodatkowe okoliczności -> patrz dalej
							break;
				case "м":	lac = "m";
							break;
				case "н":	lac = "n";
							break;
				case "о":	lac = "o";
							break;
				case "п":	lac = "p";
							break;
				case "р":	lac = "r";
							break;
				case "с":	lac = "s";
							break;
				case "т":	lac = "t";
							break;
				case "у":	lac = "u";
							break;
				case "ф":	lac = "f";
							break;
				case "х":	lac = "ch";
							break;
				case "ц":	lac = "c";
							break;
				case "ч":	lac = "cz";
							break;
				case "ш":	lac = "sz";
							break;
				case "щ":	lac = "szcz";
							break;
				case "ь":	lac = "&#769;";
							break;
				case "ю":	lac = "u";		// dodatkowe okoliczności -> patrz dalej
							break;
				case "я":	lac = "a";		// dodatkowe okoliczności -> patrz dalej
							break;
				case "’":	lac = "";		// pomija się
							break;
				case " ":	lac = " ";
							break;
			}
/* samogłoski
А а
О о
И и
У у
є, і, ю, я
*/
			if(curr == "л" &&
				(next == "є" || next == "ї" || next == "і" || next == "ь" || next == "ю" || next == "я") ) {
					lac = "l";	
			}
			
			if(curr == "є") {
				if(prev == "[" || prev == " ")
					lac = "Je";
				else if(prev == "’" /*|| prev == "ь"*/ || prev == "а" || prev == "о" || prev == "и" || prev == "у" || prev == "і" || prev == "є" )
					lac = "je";	
				else if(prev == "л")
					lac = "e";
				else
					lac = "ie";
			}
			
			
			if(curr == "ю") {
				if(prev == "[" || prev == " ")
					lac = "Ju";
				else if(prev == "’" /*|| prev == "ь"*/ || prev == "а" || prev == "о" || prev == "и" || prev == "у" || prev == "і" || prev == "є" )
					lac = "ju";	
				else if(prev == "л")
					lac = "u";
				else
					lac = "iu";
			}
			
			if(curr == "я") {
				if(prev == "[" || prev == " ")
					lac = "Ja";
				else if(prev == "’" /*|| prev == "ь"*/ || prev == "а" || prev == "о" || prev == "и" || prev == "у" || prev == "і" || prev == "є" )
					lac = "ja";	
				else if(prev == "л")
					lac = "a";
				else
					lac = "ia";
			}
			
			// połączenie liter ьо oddajemy przez io
			if(curr == "ь" && next == "о")
				lac = "i";
			// połączenie liter ль oddajemy przez l
			// połączenie liter льо oddajemy przez lo
			if(prev == "л" && curr == "ь")
				lac = "";
			/*if(prev == "л" && curr == "ь" && next == "о")
				lac = "";*/	
			
			// wg Wikipedii: zapis ´ nad polskimi diakrytykami: ś, ź, ć
			// зь – ź, zi;  сь – ś, si; ць – ć, ci 
			if(curr == "ь" && prev == "н")
				lac = "";

			if(document.getElementById("setPWN78C1").checked) {
			// PWN 78.C.1 - ukraińskie nazwiska zakończone na -ий, -ський, -цький mogą otrzymywać polskie zakończenia: -y, -ski, -cki
				if(spec == 3) {
					spec = 0;
					if(next == "]" || next == " ")
						lac = "y";
					else
						lac = "yj";
				}
				if(spec == 2) {
					spec = 0;
					if(next == "]" || next == " ")
						lac = "i";
					else
						lac = "ij";
				}
				if(curr == "и" && next == "й") {
					if(spec == 4)
						spec = 2;
					else
						spec = 3;
					lac = "";
				}
				if(spec == 4 && prev == "к") {
					spec = 0;
				}
				if(curr == "ь" && next == "к" ) {
					spec = 4;
					lac = "";
				}
			}
			
			// PWN 78.C.3 - przyrostek -ич piszemy w formie -ycz
			// bez zmian
			
			// PWN 78.C.4 - przyrostek -iв w mianowniku liczby pojedynczej ma postać -iw
			// bez zmian
			
			if(curr != currCase ) {
				let tmp = "";
				for(let i = 0; i < lac.length; i++)
					if(i == 0)
						tmp += lac.charAt(i).toUpperCase();
					else
						tmp += lac.charAt(i);
				lac = tmp;
			}
			return lac;
		}
	</script>
 </body>
</html>