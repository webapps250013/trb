# trb
Zestaw narzędzi opartych o JavaScript służący do transkrypcji i/lub transliteracji pism języków obcych na łacinkę.

## Obsługiwane języki
- **ara**, arabski – transliteracja abdżanu bez dodawania samogłosek wg transkrypcji polskiej uproszczonej wg KSNG;
- **bel**, białoruski – transkrypcja polska wg KSNG oraz transliteracja ISO
- **bul**, bułgarski – transkrypcja polska wg KSNG
- **gre**, grecki – transkrypcja polska wg KSNG
- **kat**, gruziński – transkrypcja polska wg KSNG
- **kaz**, kazachski - transkrypcja polska wg KSNG, transliteracja ISO, zapis w standaryzowanej łacińce kazachskiej w wersji z 2017, 2018 i 2021 roku
- **mkd**, macedoński – transkrypcja polska wg KSNG
- **mon**, mongolski – transkrypcja polska wg KSNG
- **rus**, rosyjski – transkrypcja polska wg zasad PWN oraz KSNG
- **ukr**, ukraiński – transkrypcja polska wg zasad PWN oraz KSNG
